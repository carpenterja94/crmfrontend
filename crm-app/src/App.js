import React from "react";
import './App.css';
//import logo from './logo.png'

import { CustomerList } from './components/CustomerList'

function App () {
  return (
    <div className="App">
      <h2 className = "text-center" style={{
        backgroundColor: 'blue',
        WebkitTextFillColor: 'white'
      }}>
        Customer Relationship Managment Application
      </h2>
      <img src={require("./allstate.png").default} alt="Allstate Logo"/>
      <CustomerList />
    </div>
  );
}
  

export default App;
