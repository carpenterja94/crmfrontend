import axios from 'axios';

const CUSTOMER_API_BASE_URL = "https://localhost:8080/customers"

class CustomerService {

    getCustomers() {
        return axios.get(CUSTOMER_API_BASE_URL)
    }

}
export default new CustomerService()