import React from "react";

export class AddCustomer extends React.Component {
  constructor(props) {
    super(props);
    const edit = props.edit;
    const customer = props.editingCustomer;

    this.state = {
      id: customer.id || "",
      firstName: customer.firstName || "",
      lastName: customer.lastName || "",
      city: customer.city || "",
      zipCode: customer.zipCode || "",
      dateJoined: customer.dateJoined || "",
    };
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  componentDidUpdate(prevProps, prevState) {
    const edit = this.props.edit;
    const customer = this.props.editingCustomer;
    console.log(prevProps.id);
    console.log(this.props.id);
    if (
      prevProps.editingCustomer.id !== this.props.editingCustomer.id //&&
      // prevState.id === this.state.id &&
      //prevProps.id != this.props.id
    )
      this.setState({
        id: customer.id || "",
        firstName: customer.firstName || "",
        lastName: customer.lastName || "",
        city: customer.city || "",
        zipCode: customer.zipCode || "",
        dateJoined: customer.dateJoined || "",
      });
  }

  addCustomer() {
    let url = "http://localhost:8080/customers";
    const { callBackAdd } = this.props;
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(this.state),
    };
    fetch(url, requestOptions).then((response) => {
      callBackAdd();
    });
  }

  updateCustomer() {
    let url = "http://localhost:8080/customers";
    const { callBackAdd } = this.props;
    const requestOptions = {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(this.state),
    };
    fetch(url, requestOptions).then((response) => {
      callBackAdd();
    });
  }

  render() {
    console.log(this.state);
    return (
      <div>
        <br/>
        <form
          onSubmit={(e) => {
            e.preventDefault();
            if (this.props.edit) {
              this.updateCustomer();
            } else {
              this.addCustomer();
            }
          }}
        >
          <input
            name="firstName"
            value={this.state.firstName}
            placeholder="First Name"
            onChange={this.handleChange}
          />
          <input
            name="lastName"
            value={this.state.lastName}
            placeholder="Last Name"
            onChange={this.handleChange}
          />
          <input
            name="city"
            value={this.state.city}
            placeholder="City"
            onChange={this.handleChange}
          />
          <input
            name="zipCode"
            value={this.state.zipCode}
            placeholder="Zip Code"
            onChange={this.handleChange}
          />
          <input
            name="dateJoined"
            value={this.state.dateJoined}
            placeholder="Date Joined"
            onChange={this.handleChange}
          />
          <button type="submit">Submit</button>
        </form>
      </div>
    );
  }
}
