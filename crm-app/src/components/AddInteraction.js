import React from "react";

export class AddInteraction extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      medium: "",
      comment: "",
      resolutionStatus: "",
      recordDate: "",
    };
  }

  addInteraction() {
    let url = "http://localhost:8080/interactions";
    const { callBackAddInteraction, showingRecordCustomer } = this.props;
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ ...this.state, customer: showingRecordCustomer }),
    };
    fetch(url, requestOptions).then(() => {
      callBackAddInteraction();
    });
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    return (
      <div>
        <br />
        <form
          onSubmit={(e) => {
            e.preventDefault();
            this.addInteraction();
          }}
        >
          <input
            name="medium"
            value={this.state.medium}
            placeholder="Medium"
            onChange={this.handleChange}
          />
          <input
            name="comment"
            value={this.state.comment}
            placeholder="Comment"
            onChange={this.handleChange}
          />
          <input
            name="resolutionStatus"
            value={this.state.resolutionStatus}
            placeholder="Resolution Status"
            onChange={this.handleChange}
          />
          <input
            name="recordDate"
            value={this.state.recordDate}
            placeholder="Record Date"
            onChange={this.handleChange}
          />
          <button type="submit">Save</button>
        </form>
      </div>
    );
  }
}
