import React from "react";
import { AddCustomer } from "./AddCustomer";
import { AddInteraction } from "./AddInteraction";

export class CustomerList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      customers: [],
      isLoaded: false,
      error: null,
      edit: false,
      editingCustomer: {},
      showAdd: false,
      showAllCustomer: false,
      records: [],
      showAddInteraction: false,
      addingInteractionCustomer: {},
      showingRecordCustomer: {},
    };
  }

  // API Resquests

  // Display customers (not deleted only) - GET request
  showCustomerList() {
    let userUrl = "http://localhost:8080/customers/notdeleted";
    this.setState({ isLoaded: false });
    fetch(userUrl)
      .then((res) => res.json())
      .then(
        // handle the result
        (result) => {
          //take the return 'result' array and it use it in your state
          this.setState({ isLoaded: true, customers: result });
        },
        // handle the error
        (error) => {
          this.setState({ isLoaded: true, error: error });
        }
      );
  }

  // Display all customers (including deleted) - GET request
  showCustomerListFull() {
    let userUrl = "http://localhost:8080/customers";
    this.setState({ isLoaded: false });
    fetch(userUrl)
      .then((res) => res.json())
      .then(
        // handle the result
        (result) => {
          //take the return 'result' array and it use it in your state
          this.setState({ isLoaded: true, customers: result });
        },
        // handle the error
        (error) => {
          this.setState({ isLoaded: true, error: error });
        }
      );
  }

  // Delete a customer - GET request
  deleteCustomer(id) {
    let url = "http://localhost:8080/customers/delete/" + id;
    this.setState({ isLoaded: false });
    fetch(url).then(
      // handle the result
      () => {
        this.showCustomerList();
        this.setState({ isLoaded: true });
      },
      // handle the error
      (error) => {
        this.setState({ isLoaded: true, error: error });
      }
    );
  }

  // Display customer's record - GET request
  showRecord(customer) {
    let customerId = customer.id;
    let url = "http://localhost:8080/interactions/customer/" + customerId;
    this.setState({ isLoaded: false, showingRecordCustomer: customer });

    fetch(url)
      .then((res) => res.json())
      .then(
        // handle the result
        (result) => {
          //take the return 'result' array and it use it in your state
          this.setState({
            isLoaded: true,
            records: result.content,
          });
        },
        // handle the error
        (error) => {
          this.setState({ isLoaded: true, error: error });
        }
      );
  }

  // Event Handler
  // Edit button event handler
  handleClick(customer) {
    this.setState({ edit: true, editingCustomer: customer });
  }

  // Add customer button event handler
  handleAdd = () => {
    this.setState({ showAdd: true });
  };

  // Add interaction button event handler
  handleAddInteraction = () => {
    this.setState({ showAddInteraction: true });
  };

  // Check box event handler
  handleCheckBox(e) {
    this.setState({ showAllCustomer: e.target.checked });
  }

  // Call back functions

  callBackAdd = () => {
    this.showCustomerList();
    this.setState({ showAdd: false, edit: false });
  };

  callBackAddInteraction = () => {
    this.showRecord(this.state.showingRecordCustomer);
    this.setState({ showAddInteraction: false });
  };

  componentDidMount() {
    this.showCustomerList();
  }

  render() {
    const {
      showAdd,
      edit,
      editingCustomer,
      showAddInteraction,
      showingRecordCustomer,
    } = this.state;

    if (this.state.error) {
      return (
        <aside>
          <h4> Error: {this.state.error.message}</h4>
        </aside>
      );
    } else if (!this.state.isLoaded) {
      return (
        <aside>
          <h4> Waiting... </h4>
          {/* <button onClick={ ()=>this.callApi() } /> */}
        </aside>
      );
    } else {
      return (
        <section>
          <div className="row"></div>
          <h3> Customer Data</h3>
          <button onClick={this.handleAdd}>Add Customer</button>
          {(showAdd || edit) && (
            <AddCustomer
              callBackAdd={this.callBackAdd}
              edit={edit}
              editingCustomer={editingCustomer}
            />
          )}

          <div className="row"></div>
          <br />
          <table className="table table-striped table-bordered">
            <thead
              style={{
                color: "white",
                backgroundColor: "black",
              }}
            >
              <tr>
                <th>Customer ID </th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>City</th>
                <th>Zip Code</th>
                <th>Date Joined</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {this.state.customers.map((customer) => (
                <tr key={customer.id}>
                  <th>{customer.id}</th>
                  <th>{customer.firstName}</th>
                  <th>{customer.lastName}</th>
                  <th>{customer.city}</th>
                  <th>{customer.zipCode}</th>
                  <th>{customer.dateJoined}</th>
                  <th>
                    <button onClick={() => this.handleClick(customer)}>
                      Edit
                    </button>
                    <button onClick={() => this.deleteCustomer(customer.id)}>
                      Delete
                    </button>
                    <button onClick={() => this.showRecord(customer)}>
                      Records
                    </button>
                  </th>
                </tr>
              ))}
            </tbody>
          </table>

          <hr />
          <h3>Records</h3>
          <button onClick={this.handleAddInteraction}>Add Record</button>
          {showAddInteraction && (
            <AddInteraction
              callBackAddInteraction={this.callBackAddInteraction}
              showingRecordCustomer={showingRecordCustomer}
            />
          )}
          <br />

          <div className="row"></div>
          <br />
          <table className="table table-striped table-bordered">
            <thead
              style={{
                backgroundColor: "black",
                color: "white",
              }}
            >
              <tr>
                <th>Customer ID </th>
                <th>Record ID</th>
                <th>Medium</th>
                <th>Comment</th>
                <th>ResolutionStatus</th>
                <th>Record Date</th>
              </tr>
            </thead>
            <tbody>
              {this.state.records.map((record) => (
                <tr key={record.id}>
                  <th>{record.customer.id}</th>
                  <th>{record.id}</th>
                  <th>{record.medium}</th>
                  <th>{record.comment}</th>
                  <th>{record.resolutionStatus}</th>
                  <th>{record.recordDate}</th>
                </tr>
              ))}
            </tbody>
          </table>
        </section>
      );
    }
  }
}
